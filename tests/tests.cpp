#include <iostream>
#include <gtest/gtest.h>
#include "include/add.h"

TEST(test_add, Static)
{
    EXPECT_EQ(add(3, 5), 8);
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}